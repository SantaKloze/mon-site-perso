$(document).ready(function() {
    var now = new Date()
    $('#hourSpan').text(now.getHours());
    $('#minuteSpan').text(now.getMinutes());
    $('#daySpan').text(now.getDate());
    $('#monthSpan').text(parseInt(now.getMonth()) + 1);
    $('#yearSpan').text(now.getFullYear());
});