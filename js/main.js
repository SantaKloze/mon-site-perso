$(document).ready(function () {

    $('#start-menu-modal').hide();
    $(".cvContainer").hide();
    $(".linkContainer").hide();
    $(".twitterContainer").hide();
    $(".homeContainer").hide();
    $('#schoolRelateInfos').hide();
    $('#twitterRelateInfos').hide();
    $('#wiissleRelateInfos').hide();

    // setTimeout(() => {
        $(".homeContainer").fadeIn();
    // }, 1000);

    console.log($("#cvContainer"));
    console.log($("#linkContainer"));
    console.log($("#twitterContainer"));
    console.log($("#homeContainer"));

    $('#start-menu img').click(function(event){
        console.log('clicked menu');
        $('#start-menu-modal').toggle();
    });

    $('.desktop').click(function(event){
        console.log('clicked desktop');
        $('#start-menu-modal').hide();
    });
    
    $('#notifications').click(function(){
        $('#start-menu-modal').hide();
   })

    $('.deskDiv').click(function(){
        $('#start-menu-modal').hide();
   })

    $( function() {
        $(".cvContainer").draggable({containment: "parent"});
        $(".linkContainer").draggable({containment: "parent"});
        $(".twitterContainer").draggable({containment: "parent"});
        $(".homeContainer").draggable({containment: "parent"});
    });
// ============================= CV ==================================== //
    $('#cv').click(function(event){
        event.preventDefault();
        $('*').removeClass('hightZIndex');
        $('*').removeClass('lowZIndex');
        $(".cvContainer").show();
        $('#cvContainer').addClass('hightZIndex');
        $('#linkContainer').addClass('lowZIndex');
        $('#twitterContainer').addClass('lowZIndex');
        $('#homeContainer').addClass('lowZIndex');
    });

    $('.buttonMoreHome').click(function(event){
        event.preventDefault();
        $(".homeContainer").fadeOut();
        $('*').removeClass('hightZIndex');
        $('*').removeClass('lowZIndex');
        $(".cvContainer").fadeIn();
        $('#cvContainer').addClass('hightZIndex');
        $('#linkContainer').addClass('lowZIndex');
        $('#twitterContainer').addClass('lowZIndex');
        $('#homeContainer').addClass('lowZIndex');
    });

    $('.wiissleChrome').click(function(event){
        event.preventDefault();
        $(".homeContainer").fadeOut();
        $('*').removeClass('hightZIndex');
        $('*').removeClass('lowZIndex');
        $(".cvContainer").fadeIn();
        $('#cvContainer').addClass('hightZIndex');
        $('#linkContainer').addClass('lowZIndex');
        $('#twitterContainer').addClass('lowZIndex');
        $('#homeContainer').addClass('lowZIndex');
    });

    $('span.closeCroix.cvc').click(function(event){
        event.preventDefault();
        $(".cvContainer").hide();
    });

    // ============================= LinkedIn ==================================== //

    $('#linkedIn').click(function(event){
        event.preventDefault();
        $(".linkContainer").show();
        $('*').removeClass('hightZIndex');
        $('*').removeClass('lowZIndex');
        $('#cvContainer').addClass('lowZIndex');
        $('#linkContainer').addClass('hightZIndex');
        $('#twitterContainer').addClass('lowZIndex');
        $('#homeContainer').addClass('lowZIndex');
    });

    $('span.closeCroix.linkc').click(function(event){
        event.preventDefault();
        $(".linkContainer").hide();
    });

// ============================= Twitter ==================================== //
    
    $('#twitter').click(function(event){
        event.preventDefault();
        $('*').removeClass('hightZIndex');
        $('*').removeClass('lowZIndex');
        $(".twitterContainer").show();
        $('#cvContainer').addClass('lowZIndex');
        $('#linkContainer').addClass('lowZIndex');
        $('#twitterContainer').addClass('hightZIndex');
        $('#homeContainer').addClass('lowZIndex');
    });

    $('span.closeCroix.twic').click(function(event){
        event.preventDefault();
        $(".twitterContainer").hide();
    });

// ============================= BlocNote ==================================== //

    $('#blocNote').click(function(event){
        event.preventDefault();
        $(".homeContainer").show();
        $('*').removeClass('hightZIndex');
        $('*').removeClass('lowZIndex');
        $('#cvContainer').addClass('lowZIndex');
        $('#linkContainer').addClass('lowZIndex');
        $('#twitterContainer').addClass('lowZIndex');
        $('#homeContainer').addClass('hightZIndex');
    });

    $('span.closeCroix.homec').click(function(event){
        event.preventDefault();
        $(".homeContainer").hide();
    });

    $('div.closeWindowHome').click(function(event){
        event.preventDefault();
        $("#homeContainer").hide();
    });

    $("#cvContainer").click(function(){
        $('*').removeClass('hightZIndex');
        $('*').removeClass('lowZIndex');
        $('#cvContainer').addClass('hightZIndex');
        $('#linkContainer').addClass('lowZIndex');
        $('#twitterContainer').addClass('lowZIndex');
        $('#homeContainer').addClass('lowZIndex');
    });

    $("#linkContainer").click(function(){
        $('*').removeClass('hightZIndex');
        $('*').removeClass('lowZIndex');
        $('#cvContainer').addClass('lowZIndex');
        $('#linkContainer').addClass('hightZIndex');
        $('#twitterContainer').addClass('lowZIndex');
        $('#homeContainer').addClass('lowZIndex');
    })

    $("#twitterContainer").click(function(){
        $('*').removeClass('hightZIndex');
        $('*').removeClass('lowZIndex');
        $('#cvContainer').addClass('lowZIndex');
        $('#linkContainer').addClass('lowZIndex');
        $('#twitterContainer').addClass('hightZIndex');
        $('#homeContainer').addClass('lowZIndex');
    })

    $("#homeContainer").click(function(){
        $('*').removeClass('hightZIndex');
        $('*').removeClass('lowZIndex');
        $('#cvContainer').addClass('lowZIndex');
        $('#linkContainer').addClass('lowZIndex');
        $('#twitterContainer').addClass('lowZIndex');
        $('#homeContainer').addClass('hightZIndex');
    });
    
    $("li.cvLi").click(function(){
        $('#cvRelateInfos').show();
        $('#schoolRelateInfos').hide();
        $('#twitterRelateInfos').hide();
        $('#twitterRelateInfos').hide();
        $('#wiissleRelateInfos').hide();
    });

    $("li.imieLi").click(function(){
        $('#schoolRelateInfos').show();
        $('#cvRelateInfos').hide();
        $('#twitterRelateInfos').hide();
        $('#wiissleRelateInfos').hide();
    });

    $("li.twitterLi").click(function(){
        $('#twitterRelateInfos').show();
        $('#cvRelateInfos').hide();
        $('#schoolRelateInfos').hide();
        $('#wiissleRelateInfos').hide();
    });
    
    $("li.wiissleLi").click(function(){
        $('#wiissleRelateInfos').show();
        $('#twitterRelateInfos').hide();
        $('#cvRelateInfos').hide();
        $('#schoolRelateInfos').hide();
    });
    
});